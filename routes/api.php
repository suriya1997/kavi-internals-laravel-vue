<?php
use App\Http\Controllers;
use App\Actions\Fortify;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user') ->group(function(){
    Route::post('/login', 'UserController@user_login');
    Route::post('/register', 'UserController@user_register');
    Route::get('/index', 'UserController@index');
    Route::get('/{id}', 'UserController@show');
    Route::get('/', 'UserController@index');
    Route::get('/details', 'UserController@user_details');
    Route::put('/', 'UserController@update');
    Route::delete('/{id}', 'UserController@destroy');
});

Route::get('/auth-logout', 'UserController@logout');


Route::post('sendPasswordResetLink','PasswordResetRequestController@sendEmail');


Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'ResetPasswordController@reset');

Route::get('developers', 'UserController@developer_index');
Route::get('clients', 'UserController@client_index');

Route::get('/project', 'ProjectController@index');
Route::get('/project/{id}', 'ProjectController@show');
Route::post('/project', 'ProjectController@store');
Route::put('/project', 'ProjectController@update');
Route::delete('/project/{id}', 'ProjectController@delete');




