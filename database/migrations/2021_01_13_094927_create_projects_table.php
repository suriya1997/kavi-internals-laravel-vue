<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->date('startdate',$precision = 0);
            $table->date('edc');
            $table->string('developerassigned' )->nullable();
            $table->string('client')->nullable();  
            $table->string('estimatetime', $precision = 0);
            $table->string('expectedamount');
            $table->string('platform');
            $table->string('modulespresent');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
