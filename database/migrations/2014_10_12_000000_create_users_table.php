<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at');
            $table->string('password');
            $table->string('type')->nullable();
            $table->string('dateofbirth')->nullable();
            $table->string('country')->nullable();
            $table->string('skypeid')->nullable();
            $table->string('contact');
            $table->string('address')->nullable();
            $table->string('qualification')->nullable();
            $table->string('experience')->nullable();
            $table->string('skills')->nullable();
            $table->string('projectsassigned')->nullable();
            $table->string('projectsworked')->nullable();
            $table->string('salary')->nullable();
            $table->string('hourrate')->nullable();
            $table->rememberToken('my app');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
