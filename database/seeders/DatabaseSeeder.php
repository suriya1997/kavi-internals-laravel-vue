<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('users')->insert([
            'name' => 'Suriya',
            'email' => 'suriya@gmail.com',
            'type' => 'Developer',
            'contact' => '123456',
            'password' => Hash::make('12345678'),
        ]);
        DB::table('users')->insert([
            'name' => 'Ranjith',
            'email' => 'ranjith@gmail.com',
            'type' => 'Developer',
            'contact' => '123456',
            'password' => Hash::make('12345678'),
        ]);
        DB::table('users')->insert([
            'name' => 'Siteguru',
            'email' => 'info@siteguru.co.il',
            'type' => 'Client',
            'contact' => '123456',
            'password' => Hash::make('12345678'),
        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'kavisoftek@gmail.com',
            'type' => 'Admin',
            'contact' => '123456',
            'password' => Hash::make('12345678'),
        ]);
    }
}
