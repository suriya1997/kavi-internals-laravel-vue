<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $casts =[
        'name',
        'description',
        'startdate',
        'edc',
        'developerassigned',
        'client',
        'estimatetime',
        'expectedamount',
        'platform',
        'modulespresent',   
    ];
}