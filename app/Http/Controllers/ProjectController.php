<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    public function index(Request $request)
    {    
        
        $type=$_GET['type']; 
        $id=$_GET['id']; 
    
        if($type == 'Client'){
            $client = DB::table('projects')->select('*')->where('client',$id)->get();
            return $client;
        }elseif($type == 'Developer' || $type == 'Freelancer'){      
            $developer = DB::table('projects')->select('*')->whereRaw("find_in_set('$id',developerassigned)")->get();
            return $developer; 
        }else{
            return Project::all();
        }
    }
    
    public function show($id)
    {
        return Project::find($id);
    }

    public function store(Request $request)
    {  
        

        $projects = new Project();
        $projects->name = $request->input('name');
        $projects->description = $request->input('description');
        $projects->startdate = $request->input('startdate');
        $projects->edc = $request->input('edc');

        if($request->input('developerassigned') != ''){
        $developersassigned = implode(',',$request->input('developerassigned'));
        $projects->developerassigned = str_replace('"',"", $developersassigned);
        }else{
            $projects->developerassigned = $request->input('developerassigned');
        }

        $projects->estimatetime = $request->input('estimatetime');
        $projects->expectedamount = $request->input('expectedamount');
        $projects->platform = $request->input('platform');
        $projects->modulespresent = $request->input('modulespresent');
        $projects->client = $request->input('client');
        $projects->save();
        
        return response()->json([
            'status'=>true,
            'message' => 'Project Registred!!',
            'data' => [$projects],
        ]); 
        
    }

    public function update()
    {
        $projects = Project::find(request()->id);
        $projects->name = request()->name;
        $projects->description = request()->description;
        $projects->startdate = request()->startdate;
        $projects->edc = request()->edc;
        $projects->developerassigned = request()->developerassigned;
        $projects->estimatetime = request()->estimatetime;
        $projects->expectedamount = request()->expectedamount;
        $projects->platform = request()->platform;
        $projects->modulespresent = request()->modulespresent;
        $projects->client = request()->client;
        $projects->update();
        
        return 'success';
    }

    public function delete(Request $request, $id)
    {
        $projects = Project::findOrFail($id);
        $projects->delete();

        return 204;
    }
  
}
  