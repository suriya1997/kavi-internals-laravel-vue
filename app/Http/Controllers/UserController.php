<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Models\User; 
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash; 
use Laravel\Passport\TokenRepository;
use Laravel\Passport\RefreshTokenRepository;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Bridge\PersonalAccessGrant;
use League\OAuth2\Server\AuthorizationServer;
use Laravel\Passport\passport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use DB;

class UserController extends Controller
{
    public function developer_index(Request $request)
    {  
        $users = DB::table('users')->select('*')->where('type','Developer')->orWhere('type','Freelancer')->get();
        
        return $users;
    }

    public function client_index(Request $request)
    {  
        $users = DB::table('users')->select('*')->where('type','Client')->get();
        return $users;
    }
    public function index(Request $request)
    {  
        
        return User::all();
        
    }
 
    public function show(Request $request, $id)
    {
        return User::find($id);
    }
    
    public function update()
    {
        $users = User::find(request()->id);
        $users->name = request()->name;
        $users->type = request()->type;
        $users->email = request()->email;
        $users->country = request()->country;
        $users->dateofbirth = request()->dateofbirth;
        $users->contact = request()->contact;
        $users->skypeid = request()->skypeid;
        $users->address = request()->address;
        $users->qualification = request()->qualification;
        $users->skills = request()->skills;
        $users->projectsassigned = request()->projectsassigned;
        $users->projectsworked = request()->projectsworked;
        $users->salary = request()->salary;
        $users->hourrate = request()->hourrate;
        $users->update();

        return 'success';
    }

    public function destroy(Request $request, $id)
    {
        
        $users = User::findOrFail($id);
        $users->delete();

        return 204;
    }

    

     /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function user_register(Request $request) 
    { 
        $user = User::make($request->all()); 

        $getEmail = User::where('email','=',$request->email)->get();
        
        if(count($getEmail)!=0){
            
            return response()->json(['Email already registered!!']); 
        }
        else{

            $getSkypeid = User::where('skypeid','=',$request->skypeid)->get();

            if(count($getSkypeid)!=0){
            
                return response()->json(['Skypeid already registered!!']); 
            }
            else{
       
            
            $input = $request->all(); 
            
            $input['password'] =Hash::make($input['password']); 
            $user = User::create($input); 
            $success['token'] =  $user->createToken('my app')->accessToken; 
        return response()->json([
            'message'=>'Success',
            'data'=>$success,
            'user'=>$user]);
        
        }
       
        }
    }
    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function user_details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function user_login(Request $request){

        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){

            $user = $request->user();
            $token= $user->createToken('Laravel')->accessToken;
            $status['name'] =  $user->name;                
            $value = $request->session()->getId();
            
            return response()->json([
                'session_id' => $value,
                'access_token' => $token, 
                'user' =>$user,
                ]);   
        }
        else{
            return response()->json(['error' => 'Unauthenticated.'], 401);
            }       
    }

        /** 
     * logout api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function logout(Request $request)
    {   
        if (Auth::check()) {            
            auth()->logout(); 
            
            return response()->json(['success' => 'Logout_Success'],200); 
        }
        
        
    }       
}
        
        





    
        